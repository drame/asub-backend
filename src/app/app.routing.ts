import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {GuardService} from './guard.service';

// Import Containers
import {
  FullLayout,
  SimpleLayout
} from './containers';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '',
    component: FullLayout,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: './views/dashboard/dashboard.module#DashboardModule',
        canActivate:[GuardService]
        
      },
      {
        path: 'components',
        loadChildren: './views/components/components.module#ComponentsModule'
      },
      {
        path: 'icons',
        loadChildren: './views/icons/icons.module#IconsModule'
      },
      {
        path: 'widgets',
        loadChildren: './views/widgets/widgets.module#WidgetsModule'
      },
      {
        path: 'utilisateurs',
        loadChildren: './views/utilisateurs/utilisateurs.module#UtilisateursModule',
        canActivate:[GuardService]
      },
      {
        path: 'structures',
        loadChildren: './views/structures/structures.module#StructuresModule',
        canActivate:[GuardService]
      },
      {
        path: 'donnees',
        loadChildren: './views/donnees/donnees.module#DonneesModule',
        canActivate:[GuardService]
      },
      {
        path: 'articles',
        loadChildren: './views/articles/articles.module#ArticlesModule',
        canActivate:[GuardService]
      },
      {
        path: 'article',
        loadChildren: './views/article/article.module#ArticleModule',
        canActivate:[GuardService]
      },
      {
        path: 'evenements',
        loadChildren: './views/evenements/evenements.module#EvenementsModule',
        canActivate:[GuardService]
      },
      {
        path: 'evenement',
        loadChildren: './views/evenement/evenement.module#EvenementModule',
        canActivate:[GuardService]
      },
      {
        path: 'publicite',
        loadChildren: './views/publicite/publicite.module#PubliciteModule',
        canActivate:[GuardService]
      },
      {
        path: 'publicites',
        loadChildren: './views/publicites/publicites.module#PublicitesModule',
        canActivate:[GuardService]
      },
      {
        path: 'taux',
        loadChildren: './views/taux/taux.module#TauxModule',
        canActivate:[GuardService]
      },
      {
        path: 'charts',
        loadChildren: './views/chartjs/chartjs.module#ChartJSModule'
      }
    ]
  },
  {
    path: 'pages',
    component: SimpleLayout,
    data: {
      title: 'Pages'
    },
    children: [
      {
        path: '',
        loadChildren: './views/pages/pages.module#PagesModule',
        canActivate:[GuardService]
      }
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
