import { Injectable } from '@angular/core';
import { Http,Headers,Response,RequestOptions,URLSearchParams} from '@angular/http';
import {baseUrl} from './config';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Rx';
@Injectable()
export class AsubService {
  private headers;
  constructor(private http:Http) { }
  // structures
  getStructures(data){
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
    let options = new RequestOptions({ headers: this.headers,withCredentials:true });
    let url=baseUrl+"/structures?size="+data.size+"&page="+data.page;
    return this.http.get(url,options)
    .map((res)=> res.json())
    .catch((error:Response)=>{
      return Observable.throw(error.json().error || 'Server error');
    });
  }
  getStructures1(){
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
    let options = new RequestOptions({ headers: this.headers,withCredentials:true });
    let url=baseUrl+"/allStructures";
    return this.http.get(url,options)
    .map((res)=> res.json())
    .catch((error:Response)=>{
      return Observable.throw(error.json().error || 'Server error');
    });
  }
  getStructure(data){
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
    let options = new RequestOptions({ headers: this.headers,withCredentials:true });
    let url=baseUrl+"/structure?s="+data;
    return this.http.get(url,options)
    .map((res)=> res.json())
    .catch((error:Response)=>{
      return Observable.throw(error.json().error || 'Server error');
    });
  }
  editStructure(data){
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
    let options = new RequestOptions({ headers: this.headers,withCredentials:true });
    let url=baseUrl+"/structure";
    return this.http.put(url,data,options)
    .map((res)=> res.json())
    .catch((error:Response)=>{
      return Observable.throw(error.json().error || 'Server error');
    });
  }
  addStructure(data){
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
    let options = new RequestOptions({ headers: this.headers,withCredentials:true });
    let url=baseUrl+"/structure";
    return this.http.post(url,data,options)
    .map((res)=> res.json())
    .catch((error:Response)=>{
      return Observable.throw(error.json().error || 'Server error');
    });
  }
  deleteStructure(data){
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
    let options = new RequestOptions({ headers: this.headers,withCredentials:true });
    let url=baseUrl+"/structure?s="+data;
    return this.http.delete(url,options)
    .map((res)=> res.json())
    .catch((error:Response)=>{
      return Observable.throw(error.json().error || 'Server error');
    });
  }
  // donnees
  getDonnees(data){
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
    let options = new RequestOptions({ headers: this.headers,withCredentials:true });
    let url=baseUrl+"/donnees?size="+data.size+"&page="+data.page+"&s="+data.symboleStructure;
    return this.http.get(url,options)
    .map((res)=> res.json())
    .catch((error:Response)=>{
      return Observable.throw(error.json().error || 'Server error');
    });
  }
  getDonnee(data){
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
    let options = new RequestOptions({ headers: this.headers,withCredentials:true });
    let url=baseUrl+"/donnee?id="+data;
    return this.http.get(url,options)
    .map((res)=> res.json())
    .catch((error:Response)=>{
      return Observable.throw(error.json().error || 'Server error');
    });
  }
  editDonnee(data){
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
    let options = new RequestOptions({ headers: this.headers,withCredentials:true });
    let url=baseUrl+"/donnee";
    return this.http.put(url,data,options)
    .map((res)=> res.json())
    .catch((error:Response)=>{
      return Observable.throw(error.json().error || 'Server error');
    });
  }
  addDonnee(data){
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
    let options = new RequestOptions({ headers: this.headers,withCredentials:true });
    let url=baseUrl+"/donnee";
    return this.http.post(url,data,options)
    .map((res)=> res.json())
    .catch((error:Response)=>{
      return Observable.throw(error.json().error || 'Server error');
    });
  }
  deleteDonnee(data){
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
    let options = new RequestOptions({ headers: this.headers,withCredentials:true });
    let url=baseUrl+"/donnee?id="+data;
    return this.http.delete(url,options)
    .map((res)=> res.json())
    .catch((error:Response)=>{
      return Observable.throw(error.json().error || 'Server error');
    });
  }
  importExcelFile(fileList){
    if(fileList.length > 0) {
      let file: File = fileList[0];
      let formData:FormData = new FormData();
      formData.append('file', file, file.name);
    this.headers = new Headers();
    //this.headers.append('Content-Type','');
    this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
    let options = new RequestOptions({ headers: this.headers,withCredentials:true });
    let url=baseUrl+"/import";
    return this.http.post(url,formData,options)
    .map((res)=> res.json())
    .catch((error:Response)=>{
      return Observable.throw(error.json().error || 'Server error');
    });
  }
}
addArticle(data,file){
  this.headers = new Headers();
  //this.headers.append('Content-Type', 'application/json');
  this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
  let options = new RequestOptions({ headers: this.headers,withCredentials:true });
  let url=baseUrl+"/article";
  let formData:FormData = new FormData();
  formData.append('photo', file?file:"", file?file.name:"");

  formData.append('article', new Blob([JSON.stringify(data)], {
    type: "application/json"
}));
  return this.http.post(url,formData,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
getArticles(data){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
  let options = new RequestOptions({ headers: this.headers,withCredentials:true });
  let url=baseUrl+"/articles?type="+data.type+"&page="+data.page+"&size="+data.size;
  return this.http.get(url,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
deleteArticle(data){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
  let options = new RequestOptions({ headers: this.headers,withCredentials:true });
  let url=baseUrl+"/article?id="+data;
  return this.http.delete(url,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
getArticle(data){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
  let options = new RequestOptions({ headers: this.headers,withCredentials:true });
  let url=baseUrl+"/article?id="+data;
  return this.http.get(url,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
changeStatus(id,s){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
  let options = new RequestOptions({ headers: this.headers,withCredentials:true });
  let url=baseUrl+"/article?id="+id+"&v="+s;
  return this.http.put(url,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
addEvenement(data,file){
  this.headers = new Headers();
  //this.headers.append('Content-Type', 'application/json');
  this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
  let options = new RequestOptions({ headers: this.headers,withCredentials:true });
  let url=baseUrl+"/evenement";
  let formData:FormData = new FormData();
  formData.append('photo', file?file:"", file?file.name:"");

  formData.append('evenement', new Blob([JSON.stringify(data)], {
    type: "application/json"
}));
  return this.http.post(url,formData,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
getEvenements(data){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
  let options = new RequestOptions({ headers: this.headers,withCredentials:true });
  let url=baseUrl+"/evenements?type="+data.type+"&page="+data.page+"&size="+data.size;
  return this.http.get(url,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
deleteEvenement(data){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
  let options = new RequestOptions({ headers: this.headers,withCredentials:true });
  let url=baseUrl+"/evenement?id="+data;
  return this.http.delete(url,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
getEvenement(data){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
  let options = new RequestOptions({ headers: this.headers,withCredentials:true });
  let url=baseUrl+"/evenement?id="+data;
  return this.http.get(url,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
changeEvenementStatus(id,s){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
  let options = new RequestOptions({ headers: this.headers,withCredentials:true });
  let url=baseUrl+"/evenement?id="+id+"&v="+s;
  return this.http.put(url,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
addPublicite(data,file){
  this.headers = new Headers();
  //this.headers.append('Content-Type', 'application/json');
  this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
  let options = new RequestOptions({ headers: this.headers,withCredentials:true });
  let url=baseUrl+"/publicite";
  let formData:FormData = new FormData();
  formData.append('photo', file?file:"", file?file.name:"");

  formData.append('publicite', new Blob([JSON.stringify(data)], {
    type: "application/json"
}));
  return this.http.post(url,formData,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
getPublicites(data){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
  let options = new RequestOptions({ headers: this.headers,withCredentials:true });
  let url=baseUrl+"/publicites?page="+data.page+"&size="+data.size;
  return this.http.get(url,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
deletePublicite(data){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
  let options = new RequestOptions({ headers: this.headers,withCredentials:true });
  let url=baseUrl+"/publicite?id="+data;
  return this.http.delete(url,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
getPublicite(data){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
  let options = new RequestOptions({ headers: this.headers,withCredentials:true });
  let url=baseUrl+"/publicite?id="+data;
  return this.http.get(url,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
changePubliciteStatus(id,s){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
  let options = new RequestOptions({ headers: this.headers,withCredentials:true });
  let url=baseUrl+"/publicite?id="+id+"&v="+s;
  return this.http.put(url,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
image(i){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
  let options = new RequestOptions({ headers: this.headers,withCredentials:true });
  let url=baseUrl+"/image?photo="+i;
  return this.http.get(url,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
// utilisateurs

getUtilisateurs(data){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
  let options = new RequestOptions({ headers: this.headers,withCredentials:true });
  let url=baseUrl+"/users?size="+data.size+"&page="+data.page;
  return this.http.get(url,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}

getAllUtilisateurs(){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
  let options = new RequestOptions({ headers: this.headers,withCredentials:true });
  let url=baseUrl+"/users/get";
  return this.http.get(url,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
getUtilisateur(data){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
  let options = new RequestOptions({ headers: this.headers,withCredentials:true });
  let url=baseUrl+"/user?id="+data;
  return this.http.get(url,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
login(credential){
  let headers = new Headers({ 
    //'Authorization': 'Basic ' + btoa(credential.username + ':' + credential.password),
    'X-Requested-With': 'XMLHttpRequest' // to suppress 401 browser popup
});
  headers.append('Content-Type', 'application/json');
  console.log(credential);
  //this.headers.append('Authorization', 'Basic ' + btoa(credential.username + ':' + credential.password));
 let params=new URLSearchParams();
 params.append('email',credential.email);
 params.append('password',credential.password); 
 let options = new RequestOptions({ headers: headers,params:params,withCredentials:true});
  let url=baseUrl+"/login";
  return this.http.post(url,{},options)
  .map((res)=> {
    console.log(res);
  return res.json();
})
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
logout(){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
  //this.headers.append('Authorization', 'Basic ' + btoa(credential.username + ':' + credential.password));
  let options = new RequestOptions({ headers: this.headers,withCredentials:true});
  let url=baseUrl+"/logout";
  return this.http.get(url,options)
  .map((res)=> {
    console.log(res);
  return res.json()})
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
getUser(){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
  let options = new RequestOptions({ headers: this.headers,withCredentials:true });
  let url=baseUrl+"/user"
  return this.http.get(url,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
getTaux(){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
  let options = new RequestOptions({ headers: this.headers,withCredentials:true });
  let url=baseUrl+"/taux"
  return this.http.get(url,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
deleteAllStructures(){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
  let options = new RequestOptions({ headers: this.headers,withCredentials:true });
  let url=baseUrl+"/structures"
  return this.http.delete(url,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
updateTaux(taux){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
  let options = new RequestOptions({ headers: this.headers,withCredentials:true });
  let url=baseUrl+"/taux/update"
  return this.http.post(url,taux,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
addUtilisateur(data){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
  let options = new RequestOptions({ headers: this.headers,withCredentials:true });
  let url=baseUrl+"/user";
  return this.http.post(url,data,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
deleteUtilisateur(data){
  this.headers = new Headers();
  this.headers.append('Content-Type', 'application/json');
  this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
  let options = new RequestOptions({ headers: this.headers,withCredentials:true });
  let url=baseUrl+"/user?id="+data;
  return this.http.delete(url,options)
  .map((res)=> res.json())
  .catch((error:Response)=>{
    return Observable.throw(error.json().error || 'Server error');
  });
}
}
