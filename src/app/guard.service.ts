import { Injectable } from '@angular/core';
import { CanActivate,ActivatedRouteSnapshot ,RouterStateSnapshot,Router} from '@angular/router';
import {AsubService} from './asub.service';
@Injectable()
export class GuardService implements CanActivate {

  constructor(public service:AsubService,public router:Router) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
   return this.service.getUser().map(res=>{
     console.log(state.url);
     if(res.login==true&&res.role=="ADMIN"){
       console.log("connected")
      if(state.url=="/pages/login"){
        console.log("prev login")
        return false;
      }
       return true;
     }
     else{
      console.log("not connected")
       if(state.url=="/pages/login"){
        return true;
       }
       else{
         this.router.navigate(['/pages/login']);
         return false
       }
     }
   }).first();
  }
}
