import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { DonneesComponent } from './donnees.component';
import { DonneesRoutingModule } from './donnees-routing.module';
import { FormsModule } from '@angular/forms';
import {ToastModule} from 'ng2-toastr/ng2-toastr';
import {ToastOptions} from 'ng2-toastr';
import { ModalModule } from "ngx-bootstrap";

export class CustomOption extends ToastOptions {
  animate = 'flyRight'; // you can override any options available
  newestOnTop = false;
  showCloseButton = true;
  positionClass='toast-bottom-right';
}
@NgModule({
  imports: [
    DonneesRoutingModule,
    ChartsModule,
    BsDropdownModule,FormsModule,CommonModule,ToastModule.forRoot(),ModalModule.forRoot()
  ],
  declarations: [ DonneesComponent ],
  providers:[{provide: ToastOptions, useClass: CustomOption}]
})
export class DonneesModule { }
