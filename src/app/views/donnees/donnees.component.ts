import { Component,OnInit,ViewContainerRef,ViewChild } from '@angular/core';
import { AsubService} from './../../asub.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ModalDirective } from 'ngx-bootstrap/modal/modal.component';
import {ActivatedRoute,Router} from '@angular/router';
import * as moment from 'moment';
@Component({
  templateUrl: 'donnees.component.html'
})
export class DonneesComponent implements OnInit {
  public donnees={last:false,content:[{symbole:"",var:"",volTitre:"",volFcfa:"",date:""}],first:false};
  public donnee={id:0,structure:{symbole:""},var:"",volTitre:"",volFcfa:"",date:""};
  public  options={size:10,page:0,symboleStructure:""};
  public pages=[];
  public pageCourante=0;
  public idDonneeToDelete;
  public structures=[];
  @ViewChild('suppressionModal')
  public suppressionModal:ModalDirective;
  ngOnInit(): void {
    this.route.queryParams.subscribe(params=>{
      this.options.symboleStructure=params['s'];
    if(this.options.symboleStructure.length>0){
        this.donnee.structure.symbole=this.options.symboleStructure;

        this.getDonnees();
      }

    });
      this.service.getStructures1().subscribe(res=>{
        this.structures=res;
        if(this.options.symboleStructure.length>0){

        }
        else{
          this.options.symboleStructure=this.structures[0].symbole;
        }
      });
  }

  constructor(public service:AsubService,public router:Router, public toastr: ToastsManager, vcr: ViewContainerRef,public route:ActivatedRoute) {
    this.toastr.setRootViewContainerRef(vcr);
 }
  public brandPrimary = '#20a8d8';
  public brandSuccess = '#4dbd74';
  public brandInfo = '#63c2de';
  public brandWarning = '#f8cb00';
  public brandDanger = '#f86c6b';

  // dropdown buttons
  public status: { isopen } = { isopen: false };
  public toggleDropdown($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }
addDonnee(){
this.service.addDonnee(this.donnee).subscribe(res=>{
  if(res.error==false){
    this.showSuccess("La donnée est bien ajoutée !");
  this.getDonnees();
  }
  else{
    this.showError(res.structure);
  }
})
}
getDonnees(){
 this.service.getDonnees(this.options).subscribe(res=>{
   res.content.map(el=>{
     el.date=moment(el.date).format("YYYY-MM-DD");
     el.var=Math.round(el.var * 100) / 100;
     return el;
   });
   this.donnees=res;
   this.pages=new Array(res.totalPages);
 }) 
}
changeSize(){
  this.getDonnees();
}
paginate(p){
  if(p=='p')
    {
      p=this.options.page-1;
    }
    else if(p=='n'){
      p=this.options.page+1;
    }
  this.options.page=p;
  this.getDonnees();
}
showSuccess(m) {
  this.toastr.success(m, 'Success!');
}

showError(m) {
  this.toastr.error(m, 'Oops!');
}

showWarning(m) {
  this.toastr.warning('You are being warned.', 'Alert!');
}

showInfo(m) {
  this.toastr.info('Just some information for you.');
}

showCustom(m) {
  this.toastr.custom(m, null, {enableHTML: true});
}
editerDonnee(s){
this.donnee=s;
}
showSuppressionModal(id){
  this.idDonneeToDelete=id;
  this.suppressionModal.show();
}
deleteDonnee(){
  this.service.deleteDonnee(this.idDonneeToDelete).subscribe(res=>{
    this.suppressionModal.hide();
    this.showSuccess("Donnée  supprimée !");
    this.getDonnees();
  });
}
changeStructure(){
  this.options.symboleStructure=this.donnee.structure.symbole;
  this.getDonnees();
}
}
