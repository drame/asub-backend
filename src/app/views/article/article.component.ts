import { Component,OnInit,ViewContainerRef,ViewChild,ElementRef,ViewEncapsulation } from '@angular/core';
import { AsubService} from './../../asub.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ModalDirective } from 'ngx-bootstrap/modal/modal.component';
import {ActivatedRoute,Router} from '@angular/router';

@Component({
  templateUrl: 'article.component.html',
  styleUrls:[ './../froala_style.css'],
})
export class ArticleComponent implements OnInit {
  public article:any={auteur:{}};
  public users:any=[];
  public structures:any=[];
  public url:any;
  public loading = false;
  public file: FileList; 
  public baseImageUrl;
  ngOnInit(): void {
    this.route.queryParams.subscribe(params=>{
      
    if(params['id']>0){
        this.getArticle(params['id']);
  
      }
  

    });
    this.getUtilisateurs();
    this.getStructures();
  }

  constructor(public service:AsubService,public toastr: ToastsManager, vcr: ViewContainerRef,public router:Router,public route:ActivatedRoute) {
    this.toastr.setRootViewContainerRef(vcr);
 }
  public brandPrimary = '#20a8d8';
  public brandSuccess = '#4dbd74';
  public brandInfo = '#63c2de';
  public brandWarning = '#f8cb00';
  public brandDanger = '#f86c6b';

  // dropdown buttons
  public status: { isopen } = { isopen: false };
  public toggleDropdown($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }
  getArticle(id){
    this.service.getArticle(id).subscribe(res=>{
      this.article=res;
      if(this.article.auteur==null){
        this.article.auteur={};
      }
      this.baseImageUrl="http://localhost:8081/image?photo="+this.article.photoUrl;
    })
  }
  addArticle(){
    console.log(this.article)
    this.loading=true;
  this.service.addArticle(this.article,this.file).subscribe(res=>{
   
    if(res.error==false){
      this.loading=false;
      this.article.contenu="";
      this.article={};
      this.article.auteur={};
      this.showSuccess("L'article est bien ajouté !");
    }
    else{
      let message=res.titre?+"Le titre "+ res.titre:(res.type?+"Le type "+res.type:(res.contenu?+"Le contenu "+res.contenu:""));
      this.showError(message);
      this.loading=false;
    }
  },err=>{
    this.loading=false;
    this.showError("Une erreur est survenue !!");
  })
  }
  getUtilisateurs(){
    this.service.getAllUtilisateurs().subscribe(res=>{
      this.users=res;
    })
  }
  getStructures(){
    this.service.getStructures1().subscribe(res=>{
      this.structures=res;
    })
  }
showSuccess(m) {
  this.toastr.success(m, 'Success!');
}

showError(m) {
  this.toastr.error(m, 'Oops!');
}

showWarning(m) {
  this.toastr.warning('You are being warned.', 'Alert!');
}

showInfo(m) {
  this.toastr.info('Just some information for you.');
}

showCustom(m) {
  this.toastr.custom(m, null, {enableHTML: true});
}
readImage(event:any) {
  if (event.target.files && event.target.files[0]) {
    this.file= event.target.files[0]?event.target.files[0]:{};
    var reader = new FileReader();

    reader.onload = (event:any) => {
      this.url = event.target.result;
    }

    reader.readAsDataURL(event.target.files[0]);
  }
}

}
