import {Component} from '@angular/core';
import {FormGroup, AbstractControl, FormBuilder, Validators} from '@angular/forms';
import {AsubService} from './../../asub.service';
import { Router} from '@angular/router';
@Component({
  templateUrl: 'login.component.html'
})
export class LoginComponent {
  public form:FormGroup;
  public email:AbstractControl;
  public password:AbstractControl;
  public submitted:boolean = false;
  public auth:any={};
  constructor(fb:FormBuilder,public service:AsubService,public route:Router) {
    this.form = fb.group({
      'email': ['', Validators.compose([Validators.required, Validators.email])],
      'password': ['', Validators.compose([Validators.required, Validators.minLength(4)])]
    });

    this.email = this.form.controls['email'];
    this.password = this.form.controls['password'];
  }
  public onSubmit(values:Object):void {
    this.auth={};
    this.submitted = true;
    if (this.form.valid) {
      this.service.login({email:this.email.value,password:this.password.value}).subscribe(res=>{
        if(res.login==true){
          if(res.role=="ADMIN"){
            this.route.navigate(['/dashboard']);
          }
          else{
            this.auth.rep=-1;
            this.auth.message=`En tant que ${res.role} vous n'avez pas les autorisations d'accès cet espace`;
          }
         
        }
        else{
          this.auth.rep=false;
          this.auth.message=res;
        }
      })
    }
  }
}
