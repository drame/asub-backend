import { Component,OnInit,ViewContainerRef,ViewChild,ElementRef,ViewEncapsulation } from '@angular/core';
import { AsubService} from './../../asub.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ModalDirective } from 'ngx-bootstrap/modal/modal.component';
import {ActivatedRoute,Router} from '@angular/router';
import * as moment from 'moment';
@Component({
  templateUrl: 'evenement.component.html',
  styleUrls:[ './../froala_style.css'],
})
export class EvenementComponent implements OnInit {
  public evenement:any={};
  public url:any;
  public loading = false;
  public file: FileList; 
  public minDate;
  public maxDate;
  public baseImageUrl;
  ngOnInit(): void {
    this.route.queryParams.subscribe(params=>{
      
    if(params['id']>0){
        this.getEvenement(params['id']);
      }

    });
  }

  constructor(public service:AsubService,public toastr: ToastsManager, vcr: ViewContainerRef,public router:Router,public route:ActivatedRoute) {
    this.toastr.setRootViewContainerRef(vcr);
 }
  public brandPrimary = '#20a8d8';
  public brandSuccess = '#4dbd74';
  public brandInfo = '#63c2de';
  public brandWarning = '#f8cb00';
  public brandDanger = '#f86c6b';

  // dropdown buttons
  public status: { isopen } = { isopen: false };
  public toggleDropdown($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }
  getEvenement(id){
    this.service.getEvenement(id).subscribe(res=>{
      this.evenement=res;
      this.evenement.date=new Date(this.evenement.date);
      this.evenement.fin=new Date(this.evenement.fin);
      this.baseImageUrl="http://193.70.39.157:8080/asub/image?photo="+this.evenement.photoUrl;
    })
  }
  addEvenement(){
    console.log(this.evenement)
    this.loading=true;
  this.service.addEvenement(this.evenement,this.file).subscribe(res=>{
    if(res.error==false){
      this.loading=false;
      this.evenement.description="";
      this.evenement={};
      this.showSuccess("L'évenement est bien ajouté !");
    }
    else{
      let message=res.titre?+"Le titre "+ res.titre:(res.type?+"Le type "+res.type:(res.description?+"La description "+res.description:res.evenement));
      this.showError(message);
      this.loading=false;
    }
  },err=>{
    this.loading=false;
    this.showError("Une erreur est survenue !!");
  })
  }
showSuccess(m) {
  this.toastr.success(m, 'Success!');
}

showError(m) {
  this.toastr.error(m, 'Oops!');
}

showWarning(m) {
  this.toastr.warning('You are being warned.', 'Alert!');
}

showInfo(m) {
  this.toastr.info('Just some information for you.');
}

showCustom(m) {
  this.toastr.custom(m, null, {enableHTML: true});
}
readImage(event:any) {
  if (event.target.files && event.target.files[0]) {
    this.file= event.target.files[0]?event.target.files[0]:{};
    var reader = new FileReader();

    reader.onload = (event:any) => {
      this.url = event.target.result;
    }

    reader.readAsDataURL(event.target.files[0]);
  }
}

}
