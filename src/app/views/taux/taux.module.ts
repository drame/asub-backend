import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { FormsModule } from '@angular/forms';
import {ToastModule} from 'ng2-toastr/ng2-toastr';
import {ToastOptions} from 'ng2-toastr';
import { ModalModule } from "ngx-bootstrap";
import { LoadingModule, ANIMATION_TYPES } from 'ngx-loading';
import{TauxRoutingModule} from './taux-routing.module';
import{TauxComponent} from './taux.component';
export class CustomOption extends ToastOptions {
  animate = 'flyRight'; // you can override any options available
  newestOnTop = false;
  showCloseButton = true;
  positionClass='toast-bottom-right';
}
@NgModule({
  imports: [
    TauxRoutingModule,
    ChartsModule,
    BsDropdownModule,FormsModule,CommonModule,ToastModule.forRoot(),ModalModule.forRoot(),LoadingModule.forRoot({
      animationType: ANIMATION_TYPES.rotatingPlane,
      backdropBackgroundColour: 'rgba(0,0,0,0.1)', 
      backdropBorderRadius: '4px',
      primaryColour: '#ffffff', 
      secondaryColour: '#ffffff', 
      tertiaryColour: '#ffffff'
  })
  ],
  declarations: [ TauxComponent ],
  providers:[{provide: ToastOptions, useClass: CustomOption}]
})
export class TauxModule { }
