import { Component,OnInit,ViewContainerRef,ViewChild,ElementRef,ViewEncapsulation } from '@angular/core';
import { AsubService} from './../../asub.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ModalDirective } from 'ngx-bootstrap/modal/modal.component';
import {ActivatedRoute,Router} from '@angular/router';

@Component({
  templateUrl: 'taux.component.html',
})
export class TauxComponent implements OnInit {
  public utilisateurs={last:false,first:false,content:[]};
  public utilisateur={description:"",nom:"",prenom:"",password:"",email:""};
  public  options={size:10,page:0};
  public pages=[];
  public pageCourante=0;
  public idUtilisateurToDelete;
  public action='add';
  @ViewChild('fileInput') inputEl: ElementRef;
  @ViewChild('suppressionModal')
  public suppressionModal:ModalDirective;
  public loading = false;
  public taux:any=[];
  ngOnInit(): void {
     this.getTaux()
  }

  constructor(public service:AsubService,public toastr: ToastsManager, vcr: ViewContainerRef,public router:Router) {
    this.toastr.setRootViewContainerRef(vcr);
 }
  public brandPrimary = '#20a8d8';
  public brandSuccess = '#4dbd74';
  public brandInfo = '#63c2de';
  public brandWarning = '#f8cb00';
  public brandDanger = '#f86c6b';

  // dropdown buttons
  public status: { isopen } = { isopen: false };
  public toggleDropdown($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }

showSuccess(m) {
  this.toastr.success(m, 'Success!');
}

showError(m) {
  this.toastr.error(m, 'Oops!');
}

showWarning(m) {
  this.toastr.warning('You are being warned.', 'Alert!');
}

showInfo(m) {
  this.toastr.info('Just some information for you.');
}

showCustom(m) {
  this.toastr.custom(m, null, {enableHTML: true});
}
getTaux(){
  this.service.getTaux().subscribe(res=>{
    this.taux=res;
    if(this.taux.length==0){
      this.taux.push({libelle:'EUR/XOF',valeur:'0'})
      this.taux.push({libelle:'USD/XOF',valeur:'0'})
      this.taux.push({libelle:'EUR/USD',valeur:'0'})
    }
  })
}
update(){
  this.service.updateTaux(this.taux).subscribe(res=>{
    if(res.erreur==false){
      this.showSuccess(res.message);
    }
    else{
      this.showError(res.message);
    }
  })
}
}
