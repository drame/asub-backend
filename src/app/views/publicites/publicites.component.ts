import { Component,OnInit,ViewContainerRef,ViewChild,ElementRef } from '@angular/core';
import { AsubService} from './../../asub.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ModalDirective } from 'ngx-bootstrap/modal/modal.component';
import { Router} from '@angular/router';
import * as moment from 'moment';
@Component({
  templateUrl: 'publicites.component.html'
})
export class PublicitesComponent implements OnInit {
  public publicites={first:false,last:false,content:[]}
  public  options={size:10,page:0};
  public pages=[];
  public pageCourante=0;
  public idPubliciteToDelete;
  @ViewChild('suppressionModal')
  public suppressionModal:ModalDirective;
  public loading = false;
  ngOnInit(): void {
    this.getPublicites();    
  }

  constructor(public service:AsubService,public toastr: ToastsManager, vcr: ViewContainerRef,public router:Router) {
    this.toastr.setRootViewContainerRef(vcr);
 }
  public brandPrimary = '#20a8d8';
  public brandSuccess = '#4dbd74';
  public brandInfo = '#63c2de';
  public brandWarning = '#f8cb00';
  public brandDanger = '#f86c6b';

  // dropdown buttons
  public status: { isopen } = { isopen: false };
  public toggleDropdown($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }

getPublicites(){
 this.service.getPublicites(this.options).subscribe(res=>{
  res.content.map(el=>{
    el.photoUrl="http://193.70.39.157:8080/asub/image?photo="+el.photoUrl;
    return el;
  });
   this.publicites=res;
   this.pages=new Array(res.totalPages);
 }) 
}
changeSize(){
  this.getPublicites();
}
changeType(){
  this.getPublicites();
}
changePubliciteStatus(id,s){
  this.service.changePubliciteStatus(id,s).subscribe(res=>{

  });
}
paginate(p){
  if(p=='p')
    {
      p=this.options.page-1;
    }
    else if(p=='n'){
      p=this.options.page+1;
    }
  this.options.page=p;
  this.getPublicites();
}
showSuccess(m) {
  this.toastr.success(m, 'Success!');
}

showError(m) {
  this.toastr.error(m, 'Oops!');
}

showWarning(m) {
  this.toastr.warning('You are being warned.', 'Alert!');
}

showInfo(m) {
  this.toastr.info('Just some information for you.');
}

showCustom(m) {
  this.toastr.custom(m, null, {enableHTML: true});
}
editerPublicite(id){
  this.router.navigate(["/publicite"],{queryParams:{id:id}});
}
showSuppressionModal(s){
  this.idPubliciteToDelete=s;
  this.suppressionModal.show();
}
deletePublicite(){
  this.service.deleteEvenement(this.idPubliciteToDelete).subscribe(res=>{
    this.suppressionModal.hide();
    this.showSuccess("Publicité  supprimée !");
    this.getPublicites();
  });
}


}
