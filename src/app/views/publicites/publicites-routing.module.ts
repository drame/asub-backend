import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {PublicitesComponent } from './publicites.component';

const routes: Routes = [
  {
    path: '',
    component:PublicitesComponent,
    data: {
      title: 'Publicités'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicitesRoutingModule {}
