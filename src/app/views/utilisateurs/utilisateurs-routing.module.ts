import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UtilisateursComponent } from './utilisateurs.component';

const routes: Routes = [
  {
    path: '',
    component: UtilisateursComponent,
    data: {
      title: 'Utilisateurs'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UtilisateursRoutingModule {}
