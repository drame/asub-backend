import { Component,OnInit,ViewContainerRef,ViewChild,ElementRef,ViewEncapsulation } from '@angular/core';
import { AsubService} from './../../asub.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ModalDirective } from 'ngx-bootstrap/modal/modal.component';
import {ActivatedRoute,Router} from '@angular/router';

@Component({
  templateUrl: 'utilisateurs.component.html',
})
export class UtilisateursComponent implements OnInit {
  public utilisateurs={last:false,first:false,content:[]};
  public utilisateur={description:"",nom:"",prenom:"",password:"",email:""};
  public  options={size:10,page:0};
  public pages=[];
  public pageCourante=0;
  public idUtilisateurToDelete;
  public action='add';
  @ViewChild('fileInput') inputEl: ElementRef;
  @ViewChild('suppressionModal')
  public suppressionModal:ModalDirective;
  public loading = false;
  ngOnInit(): void {
    this.getUtilisateurs();    
  }

  constructor(public service:AsubService,public toastr: ToastsManager, vcr: ViewContainerRef,public router:Router) {
    this.toastr.setRootViewContainerRef(vcr);
 }
  public brandPrimary = '#20a8d8';
  public brandSuccess = '#4dbd74';
  public brandInfo = '#63c2de';
  public brandWarning = '#f8cb00';
  public brandDanger = '#f86c6b';

  // dropdown buttons
  public status: { isopen } = { isopen: false };
  public toggleDropdown($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }
addUtilisateur(){
this.service.addUtilisateur(this.utilisateur).subscribe(res=>{
  if(res.error==false){
    this.showSuccess("L'utilisateur est bien ajouté !");
    this.action='add';
    this.utilisateur={description:"",nom:"",prenom:"",password:"",email:""};
  this.getUtilisateurs();
  }
  else{
    this.showError(res.user);
  }
})
}
getUtilisateurs(){
 this.service.getUtilisateurs(this.options).subscribe(res=>{
   this.utilisateurs=res;
   this.pages=new Array(res.totalPages);
 }) 
}
changeSize(){
  this.getUtilisateurs();
}
paginate(p){
  if(p=='p')
    {
      p=this.options.page-1;
    }
    else if(p=='n'){
      p=this.options.page+1;
    }
  this.options.page=p;
  this.getUtilisateurs();
}
showSuccess(m) {
  this.toastr.success(m, 'Success!');
}

showError(m) {
  this.toastr.error(m, 'Oops!');
}
editerUtilisateur(u){
  this.utilisateur=u;
}
showWarning(m) {
  this.toastr.warning('You are being warned.', 'Alert!');
}

showInfo(m) {
  this.toastr.info('Just some information for you.');
}

showCustom(m) {
  this.toastr.custom(m, null, {enableHTML: true});
}
editerStructure(s){
  this.action='edit';
this.utilisateur=s;
}
showSuppressionModal(s){
  this.idUtilisateurToDelete=s;
  this.suppressionModal.show();
}
deleteUtilisateur(){
  this.service.deleteUtilisateur(this.idUtilisateurToDelete).subscribe(res=>{
    this.suppressionModal.hide();
    this.showSuccess("utilisateur  supprimé !");
    this.getUtilisateurs();
  });
}
voirUtilisateur(s){
 // this.router.navigate(["/donnees"],{queryParams:{s:s}});
}

}
