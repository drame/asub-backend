import { Component,OnInit,ViewContainerRef,ViewChild,ElementRef } from '@angular/core';
import { AsubService} from './../../asub.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ModalDirective } from 'ngx-bootstrap/modal/modal.component';
import { Router} from '@angular/router';
@Component({
  templateUrl: 'structures.component.html'
})
export class StructuresComponent implements OnInit {
  public structures={first:false,last:false,content:[]};
  public structure={nom:"",description:"",symbole:"",secteur:""};
  public  options={size:10,page:0};
  public pages=[];
  public pageCourante=0;
  public idStructureToDelete;
  public action='add';
  @ViewChild('fileInput') inputEl: ElementRef;
  @ViewChild('suppressionModal')
  public suppressionModal:ModalDirective;
  public loading = false;
  ngOnInit(): void {
    this.getStructures();    
  }

  constructor(public service:AsubService,public toastr: ToastsManager, vcr: ViewContainerRef,public router:Router) {
    this.toastr.setRootViewContainerRef(vcr);
 }
  public brandPrimary = '#20a8d8';
  public brandSuccess = '#4dbd74';
  public brandInfo = '#63c2de';
  public brandWarning = '#f8cb00';
  public brandDanger = '#f86c6b';

  // dropdown buttons
  public status: { isopen } = { isopen: false };
  public toggleDropdown($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }
addStructure(){
this.service.addStructure(this.structure).subscribe(res=>{
  if(res.error==false){
    this.showSuccess("La structure est bien ajoutée !");
    this.action='add';
    this.structure={nom:"",description:"",symbole:"",secteur:""};
  this.getStructures();
  }
  else{
    this.showError(res.structure);
  }
})
}
getStructures(){
 this.service.getStructures(this.options).subscribe(res=>{
   this.structures=res;
   this.pages=new Array(res.totalPages);
 }) 
}
changeSize(){
  this.getStructures();
}
paginate(p){
  if(p=='p')
    {
      p=this.options.page-1;
    }
    else if(p=='n'){
      p=this.options.page+1;
    }
  this.options.page=p;
  this.getStructures();
}
showSuccess(m) {
  this.toastr.success(m, 'Success!');
}

showError(m) {
  this.toastr.error(m, 'Oops!');
}

showWarning(m) {
  this.toastr.warning('You are being warned.', 'Alert!');
}

showInfo(m) {
  this.toastr.info('Just some information for you.');
}

showCustom(m) {
  this.toastr.custom(m, null, {enableHTML: true});
}
editerStructure(s){
  this.action='edit';
this.structure=s;
}
showSuppressionModal(s){
  this.idStructureToDelete=s;
  this.suppressionModal.show();
}
deleteStructure(){
  if(this.idStructureToDelete=='all'){
    this.deleteAllStructures();
  }
  else{
    this.service.deleteStructure(this.idStructureToDelete).subscribe(res=>{
      this.suppressionModal.hide();
      this.showSuccess("Structure  supprimée !");
      this.getStructures();
    });
  }

}
deleteAllStructures(){
  this.service.deleteAllStructures().subscribe(res=>{
    this.suppressionModal.hide();
    this.showSuccess("Structures  supprimées !");
    this.getStructures();
  });
}
voirStructure(s){
  this.router.navigate(["/donnees"],{queryParams:{s:s}});
}
importExcelFile(event){
  let fileList: FileList = event.target.files;
  if(fileList.length>0){
    this.loading=true;
  }
  this.service.importExcelFile(fileList).subscribe(res=>{
    this.loading=false;
    this.showSuccess("Import terminé !");
    this.getStructures();
  });
}
}
