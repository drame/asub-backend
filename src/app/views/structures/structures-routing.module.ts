import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StructuresComponent } from './structures.component';

const routes: Routes = [
  {
    path: '',
    component: StructuresComponent,
    data: {
      title: 'Structures'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StructuresRoutingModule {}
