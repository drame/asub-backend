import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {  PubliciteComponent } from './publicite.component';

const routes: Routes = [
  {
    path: '',
    component:  PubliciteComponent,
    data: {
      title: 'Publicité'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PubliciteRoutingModule {}
