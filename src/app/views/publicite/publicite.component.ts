import { Component,OnInit,ViewContainerRef,ViewChild,ElementRef,ViewEncapsulation } from '@angular/core';
import { AsubService} from './../../asub.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ModalDirective } from 'ngx-bootstrap/modal/modal.component';
import {ActivatedRoute,Router} from '@angular/router';
import * as moment from 'moment';
@Component({
  templateUrl: 'publicite.component.html',
  styleUrls:[],
})
export class PubliciteComponent implements OnInit {
  public publicite:any={};
  public url:any;
  public loading = false;
  public file: FileList; 
  public baseImageUrl;
  ngOnInit(): void {
    this.route.queryParams.subscribe(params=>{
      
    if(params['id']>0){
        this.getPublicite(params['id']);
      }

    });
  }

  constructor(public service:AsubService,public toastr: ToastsManager, vcr: ViewContainerRef,public router:Router,public route:ActivatedRoute) {
    this.toastr.setRootViewContainerRef(vcr);
 }
  public brandPrimary = '#20a8d8';
  public brandSuccess = '#4dbd74';
  public brandInfo = '#63c2de';
  public brandWarning = '#f8cb00';
  public brandDanger = '#f86c6b';

  // dropdown buttons
  public status: { isopen } = { isopen: false };
  public toggleDropdown($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }
  getPublicite(id){
    this.service.getPublicite(id).subscribe(res=>{
      this.publicite=res;
      this.baseImageUrl="http://193.70.39.157:8080/asub/image?photo="+this.publicite.photoUrl;
    })
  }
  addPublicite(){
    console.log(this.publicite)
    this.loading=true;
  this.service.addPublicite(this.publicite,this.file).subscribe(res=>{
    if(res.error==false){
      this.loading=false;
      this.publicite.description="";
      this.publicite={};
      this.showSuccess("La publicité est bien ajoutée !");
    }
    else{
      let message=res.photoUrl?+"La photo "+ res.url:(res.url?+"L'url du site "+res.url:res.publicite);
      this.showError(message);
      this.loading=false;
    }
  },err=>{
    this.loading=false;
    this.showError("Une erreur est survenue !!");
  })
  }
showSuccess(m) {
  this.toastr.success(m, 'Success!');
}

showError(m) {
  this.toastr.error(m, 'Oops!');
}

showWarning(m) {
  this.toastr.warning('You are being warned.', 'Alert!');
}

showInfo(m) {
  this.toastr.info('Just some information for you.');
}

showCustom(m) {
  this.toastr.custom(m, null, {enableHTML: true});
}
readImage(event:any) {
  if (event.target.files && event.target.files[0]) {
    this.file= event.target.files[0]?event.target.files[0]:{};
    var reader = new FileReader();

    reader.onload = (event:any) => {
      this.url = event.target.result;
    }

    reader.readAsDataURL(event.target.files[0]);
  }
}

}
