import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {EvenementsComponent } from './evenements.component';

const routes: Routes = [
  {
    path: '',
    component:EvenementsComponent,
    data: {
      title: 'Evenements'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EvenementsRoutingModule {}
