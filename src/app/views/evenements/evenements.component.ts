import { Component,OnInit,ViewContainerRef,ViewChild,ElementRef } from '@angular/core';
import { AsubService} from './../../asub.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ModalDirective } from 'ngx-bootstrap/modal/modal.component';
import { Router} from '@angular/router';
import * as moment from 'moment';
import { first } from 'rxjs/operator/first';
import { last } from 'rxjs/operator/last';
@Component({
  templateUrl: 'evenements.component.html'
})
export class EvenementsComponent implements OnInit {
  public evenements={first:false,last:false,content:[]}
  public  options={size:10,page:0,type:""};
  public pages=[];
  public pageCourante=0;
  public idEvenementToDelete;
  @ViewChild('suppressionModal')
  public suppressionModal:ModalDirective;
  public loading = false;
  ngOnInit(): void {
    this.getEvenements();    
  }

  constructor(public service:AsubService,public toastr: ToastsManager, vcr: ViewContainerRef,public router:Router) {
    this.toastr.setRootViewContainerRef(vcr);
 }
  public brandPrimary = '#20a8d8';
  public brandSuccess = '#4dbd74';
  public brandInfo = '#63c2de';
  public brandWarning = '#f8cb00';
  public brandDanger = '#f86c6b';

  // dropdown buttons
  public status: { isopen } = { isopen: false };
  public toggleDropdown($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }

getEvenements(){
 this.service.getEvenements(this.options).subscribe(res=>{
  res.content.map(el=>{
    el.date=moment(el.date).format("DD-MM-YYYY");
    el.fin=moment(el.fin).format("DD-MM-YYYY");
    return el;
  });
   this.evenements=res;
   this.pages=new Array(res.totalPages);
 }) 
}
changeSize(){
  this.getEvenements();
}
changeType(){
  this.getEvenements();
}
changeEvenementStatus(id,s){
  this.service.changeEvenementStatus(id,s).subscribe(res=>{

  });
}
paginate(p){
  if(p=='p')
    {
      p=this.options.page-1;
    }
    else if(p=='n'){
      p=this.options.page+1;
    }
  this.options.page=p;
  this.getEvenements();
}
showSuccess(m) {
  this.toastr.success(m, 'Success!');
}

showError(m) {
  this.toastr.error(m, 'Oops!');
}

showWarning(m) {
  this.toastr.warning('You are being warned.', 'Alert!');
}

showInfo(m) {
  this.toastr.info('Just some information for you.');
}

showCustom(m) {
  this.toastr.custom(m, null, {enableHTML: true});
}
editerEvenement(id){
  this.router.navigate(["/evenement"],{queryParams:{id:id}});
}
showSuppressionModal(s){
  this.idEvenementToDelete=s;
  this.suppressionModal.show();
}
deleteEvenement(){
  this.service.deleteEvenement(this.idEvenementToDelete).subscribe(res=>{
    this.suppressionModal.hide();
    this.showSuccess("Evenement  supprimé !");
    this.getEvenements();
  });
}


}
