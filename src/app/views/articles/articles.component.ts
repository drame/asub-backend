import { Component,OnInit,ViewContainerRef,ViewChild,ElementRef } from '@angular/core';
import { AsubService} from './../../asub.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ModalDirective } from 'ngx-bootstrap/modal/modal.component';
import { Router} from '@angular/router';
import * as moment from 'moment';
@Component({
  templateUrl: 'articles.component.html'
})
export class ArticlesComponent implements OnInit {
  public articles={content:[],last:false,first:false}
  public  options={size:10,page:0,type:""};
  public pages=[];
  public pageCourante=0;
  public idArticleToDelete;
  @ViewChild('suppressionModal')
  public suppressionModal:ModalDirective;
  public loading = false;
  ngOnInit(): void {
    this.getArticles();    
  }

  constructor(public service:AsubService,public toastr: ToastsManager, vcr: ViewContainerRef,public router:Router) {
    this.toastr.setRootViewContainerRef(vcr);
 }
  public brandPrimary = '#20a8d8';
  public brandSuccess = '#4dbd74';
  public brandInfo = '#63c2de';
  public brandWarning = '#f8cb00';
  public brandDanger = '#f86c6b';

  // dropdown buttons
  public status: { isopen } = { isopen: false };
  public toggleDropdown($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }

getArticles(){
 this.service.getArticles(this.options).subscribe(res=>{
  res.content.map(el=>{
    el.creation=moment(el.creation).format("DD-MM-YYYY");
    return el;
  });
   this.articles=res;
   this.pages=new Array(res.totalPages);
 }) 
}
changeSize(){
  this.getArticles();
}
changeType(){
  this.getArticles();
}
changeStatus(id,s){
  this.service.changeStatus(id,s).subscribe(res=>{

  });
}
paginate(p){
  if(p=='p')
    {
      p=this.options.page-1;
    }
    else if(p=='n'){
      p=this.options.page+1;
    }
  this.options.page=p;
  this.getArticles();
}
showSuccess(m) {
  this.toastr.success(m, 'Success!');
}

showError(m) {
  this.toastr.error(m, 'Oops!');
}

showWarning(m) {
  this.toastr.warning('You are being warned.', 'Alert!');
}

showInfo(m) {
  this.toastr.info('Just some information for you.');
}

showCustom(m) {
  this.toastr.custom(m, null, {enableHTML: true});
}
editerArticle(id){
  this.router.navigate(["/article"],{queryParams:{id:id}});
}
showSuppressionModal(s){
  this.idArticleToDelete=s;
  this.suppressionModal.show();
}
deleteArticle(){
  this.service.deleteArticle(this.idArticleToDelete).subscribe(res=>{
    this.suppressionModal.hide();
    this.showSuccess("Article  supprimé !");
    this.getArticles();
  });
}


}
