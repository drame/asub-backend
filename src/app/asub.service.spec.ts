import { TestBed, inject } from '@angular/core/testing';

import { AsubService } from './asub.service';

describe('AsubService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AsubService]
    });
  });

  it('should be created', inject([AsubService], (service: AsubService) => {
    expect(service).toBeTruthy();
  }));
});
