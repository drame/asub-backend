import { Component, ElementRef } from '@angular/core';
import {AsubService} from './../../asub.service';
import { Router} from '@angular/router';
@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html'
})
export class AppHeader {

  constructor(public service:AsubService,public el: ElementRef,public route:Router) { }

  //wait for the component to render completely
  ngOnInit(): void {
    var nativeElement: HTMLElement = this.el.nativeElement,
    parentElement: HTMLElement = nativeElement.parentElement;
    // move all children out of the element
    while (nativeElement.firstChild) {
      parentElement.insertBefore(nativeElement.firstChild, nativeElement);
    }
    // remove the empty element(the host)
    parentElement.removeChild(nativeElement);
  }
  logout(){
    this.service.logout().subscribe(res=>{
      
    })
    this.route.navigate(['/pages/login']);
  }
}
